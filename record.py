#Grabación de un audio con el nombre output.wav
import pyaudio
import wave #permite leer y escribir archivos wav

def record_audio():
    chunk = 1024  # Guardar en trozos de 1024 pruebas
    sample_format = pyaudio.paInt16  # con 16 bits por prueba
    channels = 2 #dos altavoces
    fs = 44100  # Grabar a 44100 samples per second
    seconds = 3
    filename = "output.wav"

    p = pyaudio.PyAudio() #inicializa pyaudio

    print('Grabando...')
#abre la corriente/flujo
    stream = p.open(format=sample_format,
                    channels=channels,
                    rate=fs,#frecuencia de muestreo
                    frames_per_buffer=chunk,
                    input=True)

    frames = []  # Define 'frames' para guardar la grabacion

    # Guardar los datos en trozos de 3 segundos
    for i in range(0, int(fs / chunk * seconds)):
        data = stream.read(chunk)#captura el flujo/corriente de audio
        frames.append(data)#adjuntar a la variable 'frames'

    # Para y cierra la secuencia
    stream.stop_stream() #deja de grabar
    stream.close()#cierra

    p.terminate()

    print('Grabación finalizada.')

    # Guardar la grabación en un archivo WAV
    wf = wave.open(filename, 'wb') #inicia wave
    wf.setnchannels(channels) #asignar canales
    wf.setsampwidth(p.get_sample_size(sample_format))
    wf.setframerate(fs)
    wf.writeframes(b''.join(frames))
    wf.close()

def main():
    record_audio()
if __name__=='__main__':
    main()