#!/usr/bin/env python3

import sources
import processors
import sinks

def main():
    sound = sources.sin(40, 1500)
    print("**Original sound**")
    sinks.draw(sound, 40)
    amplified = processors.ampli(sound, 2)
    print("**Amplified sound**")
    sinks.draw(amplified, 40)

if __name__ == '__main__':
    main()