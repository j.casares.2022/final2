"""Methods for producing sound (sound sources)"""

import math

import soundfile

import config

def load(path: str):
    """Load sound from a file

    :param path: path of the file to read
    :return:     list of samples (sound)
    """

    data, fs = soundfile.read(path, dtype='int16') #me da error la librería soundfile

    # Create q list of integers
    sound = [0] * len(data)
    # Convert list of int16 to list of int
    for nsample in range(len(data)):
        sound[nsample] = data[nsample][0]

    return sound

def sin(nsamples: int, freq: float):
    """Produce a list of samples of a sinusoidal signal (sound)

    :param nsamples: number of samples
    :param freq:     frequency of the signal, in Hz (cycles/sec)
    :return:         list of samples (sound)
    """

    # Create q list of integers
    sound = [0] * nsamples

    # Compute sin samples (maximum amplitude, freq frequency)
    for nsample in range(nsamples):
        t = nsample / config.samples_second
        sound[nsample] = int(config.max_amp *
                             math.sin(2 * config.pi * freq * t))

    return sound

#Completar el módulo sources.py con un método (función),
# constant, que produzca una señal (sonido) constante
# (todas las muestras del mismo valor). La función aceptará
# dos parámetros: nsamples, que será el número de muestras a
# generar, y level, que será el nivel, el valor de cada muestra,
# un número entero. Si el valor absoluto del nivel es mayor que
# config.max_amp, se generarán muestras con el valor
# config.max_amp (si el nivel es positivo) o -config.max_amp
# (si el nivel es negativo). El resultado de la función será una
# lista de nsamples enteros.#
def constant(nsamples: int, level:int):
    sound = [0] * nsamples #lista de 0
    for nsamples in range(len(sound)):
        sound[nsamples] = level
        if level > config.max_amp:
            sound[nsamples] = config.max_amp
        elif level < -config.max_amp:
            sound[nsamples] = -config.max_amp
    return sound

#(función), square, que produzca una señal cuadrada: durante
# un número de muestras estará a la máxima amplitud, a
# continuación, durante el mismo número de muestras, a la mínima
# amplitud (negativa), luego de nuevo a la máxima, y así
# sucesivamente. La función aceptará dos parámetros: nsamples,
# que será el número de muestras a generar, y nperiod, que será
# el número de muestras de un periodo (desde que empieza la
# señal cuadrada hasta justo antes de que se repita). Esto es,
# la señal estará al máximo valor durante nperiod/2 muestras,
# luego al valor mínimo durante nperiod/2 muestras, luego de
# nuevo al máximo, y así sucesivamente. El resultado de la
# función será una lista de nsamples enteros.
def square(nsamples:int, nperiod:int):
    sound = []
    max = config.max_amp
    min  = -config.max_amp
    for i in range(nsamples):
        resto = i % nperiod #¿por qué el resto?
        if resto <= ((nperiod/2)-1):
            nuevo=max
        else:
            nuevo= min
        sound.append(nuevo)
    return sound

