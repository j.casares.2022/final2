"""Methods for storing sound, or playing it"""

import array
import config
import sounddevice



def play(sound):
    sound_array = array.array('h', [0] * len(sound))
    for nsample in range(len(sound)):
        sound_array[nsample] = sound[nsample]
    sounddevice.play(sound_array, config.samples_second)
    sounddevice.wait()

def draw(sound, max_chars: int):
    total_chars = max_chars * 2
    for nsample in range(len(sound)):
        if sound[nsample] > 0:
            print(' ' * max_chars, end='')
            nchars = int(max_chars * (sound[nsample] / config.max_amp))
            print('*' * nchars)
        else:
            nchars = int(max_chars * (-sound[nsample] / config.max_amp))
            print(' ' * (max_chars - nchars), end='')
            print('*' * nchars)

#(función), show que muestra en pantalla (usnado print) las
# muestras de una señal (sonido), como números enteros. La
# función aceptará dos parámetros: sound, que será una lista
# de muestras de sonido (enteros), y newline, que será un
# booleano indicando si se debe poner un fin de línea después
# de cada muestra. Si newline es False, se mostrarán las
# muestras como enteros separados por comas. Si es True, cada
# entero aparecerá en una línea.
def show(sound, newline: bool):
    newline = bool(newline)
    for nsamples in sound:
        if newline == False:
            print(f"{nsamples}", end=", ")
        if newline == True:
            print(f"{nsamples}\n")

#info que muestra en pantalla (usnado print) información sobre
# la señal (sonido). La función aceptará un parámetro: sound,
# que será una lista de muestras de sonido (enteros). La función
# mostrará, en el formato que se indica más abajo: el número de
# muestras del sonido, el valor máximo de las muestras (y el
# número de la última muestra que tenga ese valor), el valor
# mínimo de las muestras (y el número de la última muestra que
# tenga ese valor), el valor medio de todas las muestras, el
# número de muestras positivas, el número de muestras negativas, y el número de muesrtas con valor cero.
def info(sound):
    mean = sum(sound)/len(sound)
    pos=0
    neg=0
    null=0
    for i in sound:
        if i > 0:
            pos +=1
        elif i < 0:
            neg +=1
        else:
            null +=1



    print(f"samples: {len(sound)}\nMax value: {max(sound)} (sample {sound.index(max(sound))})\nMin value: {min(sound)} (sample {sound.index(min(sound))})\nmean value: {mean}\npositive samples: {pos}\nnegative samples: {neg}\nnull samples: {null}")
