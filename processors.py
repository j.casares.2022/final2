"""Methods for processing sound (sound processors)"""

import math

import soundfile

import config

def ampli(sound, factor: float):
    """Amplify signal (sound) by the specified factor

    :param sound:  sound (list of samples) to amplify
    :param factor: amplification factor
    :return:     list of samples (sound)
    """

    #out_sound = [0] * len(sound)
    for nsample in range(len(sound)):
        value = sound[nsample] * factor
        if value > config.max_amp:
            value = config.max_amp
        elif value < - config.max_amp:
            value = - config.max_amp
        sound[nsample] = int(value)
    return sound

# shift que "desplazará" el sonido, sumando al valor de cada
# muestra un valor de desplazamiento. Si el resultado del
# desplazamiento de una muestra es mayor que la amplitud máxima,
# o menor que la amplitud mínima, el valor de la muestra
# desplazada será ese máximo o mínimo, respectivamente. La
# función aceptará dos parámetros: sound (el sonido a desplazar,
# que será una lista de muestras de sonido) y value (el valor
# de desplazamiento, que será un número int, y que se usará
# para sumar cada muestra por él).
def shift(sound, value:int):

    for i in range(len(sound)):
        num2 = sound[i] + value
        if num2 < -config.max_amp:
            num2 = -config.max_amp
        elif num2 > config.max_amp:
            num2 = config.max_amp
        sound[i] = num2
    return sound



#trim que eliminará un cierto número de muestras de la señal
# (sonido), bien de su principio, bien de su final. Por ejemplo,
# si se indica "10", producirá una señal (sonido) igual, pero
# sin las 10 primeras o las 10 últimas muestras. Naturalmente,
# el número de muestras resultante quedará reducido en el número
# de muestras eliminadas. La función aceptará tres parámetros:
# sound (el sonido a reducir, que será una lista de muestras de
# sonido), reduction (el número de muetras a quitar, que será
# que será un número int mayor que 0), y start (un booleano que
# si es True, indica que se han de quitar las muestras del
# principio, y si es False, del final). Si reduction es mayor
# que el número de muestras en sound, se devolverá una lista
# vacía (con ninguna muestra).
def trim(sound,reduction:int,start:bool):
    if reduction >= len(sound):
        return[]
    if start == True:
        del sound[:reduction]
        return sound

    elif start == False:
        del sound[-reduction:]
        return sound

#repeat que producirá una señal (sonido) igual que la original,
# repetida el número de veces que se indique. Por ejemplo, si
# se indica una repetición de "3", se producirá un sonido que
# será igual al original, y luego tres copias de él (esto es,
# el resultante será la secuencia de cuatro veces el sonido
# original). Naturalmente, la duración del sonido quedará
# extendida con tantas muestras como tenga, multimplicada por
# el factor de repetición. La función aceptará dos parámetros:
# sound (el sonido a reducir) y factor (el factor de repetición,
# que será un número int mayor que 0).
def repeat(sound, factor:int):

    sound2 = sound * factor
    print(sound2)


def clean(sound, level: int):
    for i in range(len(sound)):
        if sound[i] and sound[-i] < level: #valor absoluto
            sound[i] = 0
    return sound

def round(sound):
    for i in range(len(sound)):
        if i == 0 or i == len(sound)-1:
            continue
        else:
            media = (sound[i] + sound[i+1] + sound[i-1]) // 3
            sound[i] = int(media)
    return sound
