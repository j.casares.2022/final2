import sinks
import sources


def fuentes():
    #global sound
    fuente = input("Dime una fuente (load, sin, constant, square):")

    if fuente == 'load':
        parametro1 = (input("¿Cómo se llama el path?: "))
        sound = sources.load(parametro1)
        #return (sound)
    elif fuente == 'sin':
        parametro1 = input("Dame los parámetros de sin.\nnsamples:")
        parametro2 = input("freq:")
        sound = sources.sin(int(parametro1), int(parametro2))
        #sumidero(sound)
        #return (sound)
    elif fuente == 'constant':
        parametro1 = input("Dame los parámetros de constant.\nnsamples:")
        parametro2 = input("level:")
        sound = sources.constant(int(parametro1), int(parametro2))
        #sumidero(sound)
        #return(sound)
    elif fuente == 'square':
        parametro1 = input("Dame los parámetros de square.\nnsamples:")
        parametro2 = input("nperiod:")
        sound = sources.square(int(parametro1), int(parametro2))
        #sumidero(sound)
        #return(sound)
    else:
        sound = fuentes()

    return(sound)

def sumidero(sound):
    sumidero = input("Dame un sink (play, draw, show, info): ")
    while True:
        if sumidero == 'play':
            sinks.play(sound)
            break
        elif sumidero == 'draw':
            max_chars = int(input('¿Cuántos carácteres?'))
            sinks.draw(sound, max_chars)
            break
        elif sumidero == 'show':
            newline = input('Newline, ¿True or False?')
            if newline == 'True':
                sinks.show(sound, True)
                break
            elif newline == 'False':
                sinks.show(sound, False)
                break
        elif sumidero == 'info':
            sinks.info(sound)
            break
        else:
            sumidero = input("Dame un sink (play, draw, show, info): ")


def main():
    #fuentes()
    sound = fuentes()
    sumidero(sound)

if __name__ == '__main__':
    main()